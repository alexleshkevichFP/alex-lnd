const toggleHeaderScrollClass = (scrollY) => {
    const header = document.querySelector('.header');

    if (scrollY > 10) {
        return header.classList.add('header--scrolled');
    }

    return header.classList.remove('header--scrolled')
}

const toggleResizeNoScrollClass = () => {
    const body = document.querySelector('body');
    const header = document.querySelector('.header');

    const isExpanded = header.classList.contains('header--expand');
    const isMobile = window.innerWidth < 992;

    console.log(isMobile, isExpanded);

    if (!isMobile && isExpanded) {
        body.classList.remove('--no-scroll')
    }

    if (isMobile && isExpanded) {
        body.classList.add('--no-scroll')
    }
}

const setResizeWatcher = () => {
    window.addEventListener('resize', () => {
        toggleResizeNoScrollClass();
    })
}

const setScrollWatcher = () => {
    window.addEventListener('scroll', () => {
        toggleHeaderScrollClass(window.scrollY);
    });
}

const setNavToggleWatcher = () => {
    const burger = document.querySelector('.burger');
    const header = document.querySelector('.header');
    const body = document.querySelector('body');

    burger.addEventListener('click', () => {
        burger.classList.toggle('burger--open');
        header.classList.toggle('header--expand');
        body.classList.toggle('--no-scroll');
    });
}

const setFeatureChangeWatcher = () => {
    const featureBtnList = document.querySelectorAll('.feature__button');
    const logo = document.querySelector('.feature__logo');

    featureBtnList.forEach((elem) => {
        elem.addEventListener('click', function() {
            console.log(this)

            featureBtnList.forEach((btn) => {
                btn.classList.remove('feature__button--active')
            });

            this.classList.add('feature__button--active');
            logo.className = `feature__logo --svg__${this.dataset.icon}`
        });
    });
}

window.addEventListener('load', () => {
    toggleHeaderScrollClass(window.scrollY);
    setScrollWatcher();
    setResizeWatcher();
    setNavToggleWatcher();
    setFeatureChangeWatcher();
})
